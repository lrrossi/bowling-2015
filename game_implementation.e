note
	description: "Summary description for {GAME_IMPLEMENTATION}."
	author: "Mattia Monga"

class
	GAME_IMPLEMENTATION

inherit

	GAME

feature

	current_score: INTEGER
		attribute
			Result := 0
		end

	is_spare: BOOLEAN
		attribute
			Result:= FALSE
		end

	is_strike: BOOLEAN
		attribute
			Result:= FALSE
		end

	last: INTEGER
		attribute
			Result := 0
		end

	extras: INTEGER
		attribute
			Result := 0
		end

	score: INTEGER
		do
			Result := current_score
		end

	roll (pins: INTEGER)
		do
			if current_roll <= 19 then
				current_roll := current_roll + 1
			else
				extras := extras - 1
			end

			current_score := current_score + pins

			if is_spare or is_strike then
				current_score := current_score + pins

				if is_strike then
					is_spare := TRUE
					is_strike := FALSE
				else
					is_spare := FALSE
				end
			end

			if pins = 10 then
				is_strike := TRUE

				if current_roll < 19 then
					current_roll := current_roll + 1

				elseif current_roll = 19 then
					current_roll := current_roll + 1
					extras := 2
				else
					extras := extras -1
				end

			elseif last + pins = 10 then
				is_spare := TRUE
			end

			if last = 0 then
				last := pins
			else
				last := 0
			end
		end

	ended: BOOLEAN
		do
			if current_roll = 20 then
				Result := True
			end
		end

feature {NONE}

	current_roll: INTEGER

invariant
	valid_roll: 0 <= current_roll and current_roll <= 20 and 0 <= extras and extras <= 2

end
